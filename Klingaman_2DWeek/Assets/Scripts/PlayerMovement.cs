﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cinemachine;

public class PlayerMovement : MonoBehaviour
{
    //Access to the player's rigidbody
    Rigidbody2D rB2D;
    //Stores the run speed of the player
    public float runSpeed;
    //Stores the jump force of the player
    public float jumpForce;
    //Access the sprite renderer of the player
    public SpriteRenderer spriteRenderer;
    //Access the animator of the player
    public Animator animator;
    //Stores the score of coins collected
    private int score;
    //Access the score text 
    public TextMeshProUGUI scoreText;
    //Access the win text
    public TextMeshProUGUI winText;

    // Start is called before the first frame update
    void Start()
    {
        //Access the rigidbody component to adjust player's rigidbody settings
        rB2D = GetComponent<Rigidbody2D>();
        //Initializes score to 0
        score = 0;
        //Initializes win text to be disabled at the start
        winText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Checks if the player has pushed the jump button
        if (Input.GetButtonDown("Jump"))
        {
            //Sets the levelmask of the level 
            int levelMask = LayerMask.GetMask("Level");
            //Creates boxcast to prevent player from jumping infinitely
            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                //Calls the jump function after checking the previous conditions
                Jump();
            }


        }
    }

    void FixedUpdate()
    {
        //Stores the horizontal input from the player
        float horizontalInput = Input.GetAxis("Horizontal");
        //Sets the velocity of the player
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        //Checks if the player is moving right
        if (rB2D.velocity.x > 0)
        {
            //Changes the sprite to face the appropriate way
            spriteRenderer.flipX = false;
        }
        else
        //Checks if the player is facing left
        if (rB2D.velocity.x < 0)
        {
            //Changes the sprite to face the appropriate way
            spriteRenderer.flipX = true;
        }
        //Checks if the player is running, and sets the correct animation
        if (Mathf.Abs(horizontalInput) > 0f)
        {
            //Changes the running boolean to true to enable running animation
            animator.SetBool("IsRunning", true);
        }
        else
        {
            //Changes the running boolean to false to disable running animation
            animator.SetBool("IsRunning", false);
        }
    }
    
    //Method to make the player jump
    void Jump()
    {
        //Sets the player's velocity, this time using the jumpForce variable
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    //Method to set the score text when coins are collected
    void SetScoreText()
    {
        //Sets the scoreText's text
        scoreText.text = "Score: " + score.ToString();

    }

    //Method to set the win text when the player enter's the win area
    void SetWinText()
    {
        //Sets the winText's text
        winText.text = "You Win! Restart the game to play again!";
        //Enables the winText TMPro object
        winText.enabled = true;

    }

    private void OnTriggerEnter2D(Collider2D other)
    {


        //Checks the tag of the gameObject to decide if it is a Coin object
        if (other.gameObject.CompareTag("Coin"))
        {
            //Deletes the gameObject from the scene after collision
            other.gameObject.SetActive(false);

            //Increases the score variable by 100 everytime a coin is collected
            score += 100;


            //Calls the SetScoreText function to see the current score after collecting coins
            SetScoreText();
        }

        //Checks if the colliding game object is the death area
        if (other.gameObject.CompareTag("DeathArea"))
        {
            //Resets the player back to the beginning
            gameObject.transform.position = new Vector3(0, 0, 0);
            
        }
        //Checks if the colliding game object is the win area
        if (other.gameObject.CompareTag("WinArea"))
        {
            //Calls the SetWinText method
            SetWinText();
        }
    }
}
